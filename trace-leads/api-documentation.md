## Методы API

Все запросы к API необходимо осуществлять в формате JSON.
JSON-payload должен передаваться в body запроса.

### Массовая загрузка заемщиков

* **Метод**

`POST`

* **Обязательные HTTP-заголовки**

`Content-type: application/json`

`Accept: application/json`

* **Адрес**

`https://trace-leads.com/api/loaners/bulk-upload`

* **Параметры JSON-запроса**

**Обязательные**

``token=[string]`` - Авторизационный токен партнера

``action=[string]`` - Действие, необходимое к выполнению для загруженных анкет. На данный момент доступно и допустимо только одно действие - **create_short_order**

``loaners=[array]`` - Массив анкет для загрузки, каждый элемент массива - анкета очередного заемщика. За один запрос возможно загрузить не более 500 анкет.

``loaners.*.phone=[string]`` - Телефон без пробелов в формате ``/^\+38[0-9]{10}$/``, например +380391234545

``loaners.*.birth_date=[string]`` - Дата рождения, в формате dd.mm.yyyy

``loaners.*.inn=[string]`` - ИНН

``loaners.*.first_name=[string]`` - Имя

``loaners.*.last_name=[string]`` - Фамилия

``loaners.*.patronymic=[string]`` - Отчество

``loaners.*.offer_type=[integer]`` - Тип оффера. Займ наличными - 1, займ на карту - 2
 
``loaners.*.amount=[integer]`` - Сумма займа

**Необязательные**

``loaners.*.email=[string]`` - Адрес электронной почты
 
``loaners.*.term=[integer]`` - Продолжительность займа
 
``loaners.*.activity_type=[integer]`` - Тип занятости.
Коммерческий сектор - 1,
Государственный сектор - 2,
Пенсионер по возрасту - 3
Пенсионер за выслугу лет - 4,
Студент - 5,
Социальная помощь - 6,
Пенсия по инвалидности - 7,
Частный предприниматель - 8

``loaners.*.passport_type=[integer]`` - Тип документа, удостоверяющего личность. Паспорт - 1, ID карта - 2 

``loaners.*.passport_series=[string]`` - Серия паспорта

``loaners.*.passport_number=[string]`` - Номер апаспорта

``loaners.*.passport_issue_date=[string]`` - Время выдачи паспорта

``loaners.*.passport_issued_by`` - Орган, выдавший паспорт

``loaners.*.id_card_number=[string]`` - номер ID карты

``loaners.*.id_card_expire_date=[string]`` - Дата истечения срока действия ID карты 

``loaners.*.sex=[integer]`` - Пол. Межской - 1, женский - 2

``loaners.*.affiliate_code=[string]`` - Идентификатор web-мастера

``loaners.*.lead_id=[string]`` - Идентификатор лида


* **Пример запроса**

```json
{ 
   "token":"kjnAS#alskmd@4asda",
   "action":"create_short_order",
   "loaners":[ 
      { 
         "phone":"+380391234545",
         "birth_date":"20.05.1992",
         "inn":"123123123123",
         "first_name":"Имя",
         "last_name":"Фамилия",
         "patronymic":"Отчество",
         "offer_type":1,
         "amount":4000,
         "email":"test@gmail.com",
         "term":30,
         "activity_type":8,
         "passport_series":"2212",
         "passport_number":"956552",
         "passport_issue_date":"19.08.2008",
         "passport_type":2,
         "passport_issued_by":"Орган выдачи паспорта",
         "id_card_number":"1233123",
         "id_card_expire_date":"20.12.2020",
         "sex":1,
         "affiliate_code":"affilfiate_code1",
         "lead_id":"iunsakd-asdkam-asdj"
      },
      { 
         "phone":"+380391234566",
         "birth_date":"12.12.1993",
         "inn":"123123123123",
         "first_name":"Сергей",
         "last_name":"Иванов",
         "patronymic":"Иванович",
         "offer_type":2,
         "amount":4500,
         "email":"test2@gmail.com",
         "term":35,
         "activity_type":1,
         "passport_series":"2213",
         "passport_number":"956552",
         "passport_issue_date":"19.08.2008",
         "passport_type":1,
         "passport_issued_by":"Орган выдачи 2",
         "id_card_number":"1233123",
         "id_card_expire_date":"20.12.2020",
         "sex":2,
         "affiliate_code":"affilfiate_code1",
         "lead_id":"iunsakd-asdkam-asdj-123"
      }
   ]
}
```

* **Успешный ответ**
    *  HTTP код - 200
``saved=[integer]`` - Количество успешно сохраненнных анкет

``skipped_loaners=[array]`` - Несохраненные анкеты, непрошедшие валидацию

``skipped_loaners.*.phone=[string]`` - Телефон анкеты, непрошедшей валидацию

``skipped_loaners.*.reason=[string]`` - Причина, по которой анкета не была сохранена

* **Пример успешного ответа** 

```json
{ 
   "saved":3,
   "skipped_loaners":[ 
      { 
         "phone":"+380391234545",
         "reason":"Loaner with phone +380391234545 already exists"
      },
      { 
         "phone":"+380011234545",
         "reason":"Bad phone +380011234545 format"
      }
   ]
}
```

* **Ошибочные ответы**
    * HTTP код 401 или 403 - неверно указан авторизационный токен
    * HTTP код 422 - неверный формат тела JSON запроса 

